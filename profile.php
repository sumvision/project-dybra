<?php
include('security/dbconnection.php');
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Project Dybra</title>

    <!-- Bootstrap -->
    <!--<link href="assets/stylesheets/bootstrap.css" rel="stylesheet">-->
    <?php
    include('css.php');
    ?>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
  <?php
  include('navigation.php');	
  ?>

  <div class="container">

    <div class="row">

      <div class="col col-md-4">
        <div class="card">
          <img src="" alt="">
          <div class="card-block">
            <h4 class="card-title">Iddo Vermeulen</h4>
            <p class="card-text">Voorman</p>
          </div>
          <ul class="list-group list-group-flush">
            <li class="list-group-item">06-12345678<br>010-1234567</li>
            <li class="list-group-item"><a href="mailto:iddovermeulen@dybra.com">iddovermeulen@dybra.com</a></li>
            <li class="list-group-item">Bergselaan 129a<br>
            3037 BG<br>
            Rotterdam</li>
            <li class="list-group-item">Medewerker #23789</li>
          </ul>
        </div>
      </div>

      <div class="col col-md-6">
         <div class="card">
          <img src="" alt="">
          <div class="card-block">
            <h4 class="card-title">Iddo Vermeulen</h4>
            <p class="card-text">Voorman</p>
          </div>
          <ul class="list-group list-group-flush">
            <li class="list-group-item">Bergselaan 129a<br>
            3037 BG<br>
            Rotterdam</li>
            <li class="list-group-item">06-12345678<br>010-1234567</li>
            <li class="list-group-item"><a href="mailto:iddovermeulen@dybra.com">iddovermeulen@dybra.com</a></li>
            <li class="list-group-item">Medewerker #23789</li>
          </ul>
        </div>
      </div>
    </div>
    
  </div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>