<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Project Dybra</title>

    <!-- Bootstrap -->
    <?php
    include('css.php');
    ?>
    <?php
    include('fonts.php');
    ?>
	
	  <!-- Chosen Autofill Style Sheets -->
  	<link rel="stylesheet" href="lib/chosen/chosen.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.js" type="text/javascript"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
  <?php
  include('navigation.php');
  ?>
  <div class="container">
    <div class="row">
      <div class="col-md-6">
        <h1>NS26376567</h1>
        <form action="processprj.php" method="post">
          <div class="form-group">
            <label for="clientName">Klantnaam</label>
          <select id="selectcustomer" data-placeholder="Selecteer klant" name="selectcustomer"> <!--class="chosen-select" tabindex="2" onchange="selectcustomer(this.value)">-->
        <option value=""></option>
        <?php
		
		include('security/dbconnection.php');

			$sql = "SELECT id, companyname, firstname, insertion, lastname FROM customers";
				$result = $conn->query($sql);
			echo "<optgroup label=\"Bedrijven\">";
			if ($result->num_rows > 0) {
				// output data of each row
				while($row = $result->fetch_assoc()) {
						if (!empty($row["companyname"]))echo "<option value=".$row["id"].">". $row["companyname"]."</option>";
				}
			} else {
				echo "0 results";
			}
			echo "</optgroup>";
			
			$sql2 = "SELECT id, companyname, firstname, insertion, lastname FROM customers";
				$result = $conn->query($sql);
			echo "<optgroup label=\"Particulieren\">";
			if ($result->num_rows > 0) {
				// output data of each row
				while($row2 = $result->fetch_assoc()) {
					if (empty($row2["companyname"])) echo "<option value=".$row2["id"].">".$row2["firstname"]." ".$row2["insertion"]. " " . $row2["lastname"]."</option>";
					//	else echo "<option value=".$row["id"].">". $row["companyname"]."</option>";
				}
			} else {
				echo "0 results";
			}
			echo "</optgroup>";
						
			// $conn->close();
			?>
        </select>
        <div id="customername"></div>
            <div class="input-group">
              <input type="text" class="form-control" placeholder="Selecteer klant">
              <span class="input-group-btn">
                <button class="btn btn-secondary" type="button">Nieuwe klant</button>
              </span>
            </div>
          </div>

          <div class="form-group">
            <div id="txtWorklocation"></div>
            <label for="clientLocation">Werklocatie</label>
            <div class="blockquote p">
              <p class="form-control-static">Kleiweg 27, 3000 AA<br>Rotterdam</p>
              <small id="clientLocation" class="form-text text-muted"><a href="#">Wijzig</a></small>
            </div>
          </div>
<?php 
      echo"<div class=\"form-group\">
        <label for=\"formGroupExampleInput5\">Voorman</label>
        <select class=\"form-control\" id=\"foreman\" name=\"foreman\">
		<option value=\"0\">Selecteer voorman</option>";
						$sql3 = "SELECT * FROM workforce";
						$result = $conn->query($sql3);
						if($result->num_rows > 0) {
							while($row3 = $result->fetch_assoc()) {
								echo"<option value=".$row3["id"].">".$row3["firstname"]." ".$row3["insertion"]. " " . $row3["lastname"]."</option>";
							}							
						}else { echo"<option>No results</option>";}
	echo"
        </select>
		</div>";
?>

      <div class="form-group">
        <label for="exampleInputEmail1">Start- en einddatum</label>
        <div class="form-inline">
          <input type="date" class="form-control" id="startdate" name="startdate"><span>-</span><input type="date" class="form-control" id="enddate" name="enddate">
        </div>
      </div>

<?php 
      echo"<div class=\"form-group\">
        <label for=\"formGroupExampleInput5\">Status</label>
        <select class=\"form-control\" id=\"status\" name=\"status\">
		<option value=\"0\">Selecteer Status</option>";
						$sql4 = "SELECT * FROM status";
						$result = $conn->query($sql4);
						if($result->num_rows > 0) {
							while($row4 = $result->fetch_assoc()) {
								echo"<option value=".$row4["id"].">".$row4["status"]."</option>";
							}							
						}else { echo"<option>No results</option>";}
	echo"
        </select>
		</div>";
?>
      
      <div class="form-group">
        <label for="preCalculation">Voorcalculatie</label>
        <div>
          <button type="button" class="btn btn-primary">Maak voorcalculatie</button>
          <small id="preCalculation" class="form-text text-muted"><i>Optioneel</i></small>
        </div>
      </div>

      <div class="form-group">
        <label for="Quotation">Offerte</label>
        <div>
          <button type="button" class="btn btn-primary">Maak offerte</button>
          <small id="Quotation" class="form-text text-muted"><i>Optioneel</i></small>
        </div>
      </div>

      <div class="form-group">
        <label for="exampleTextarea">Opmerkingen</label>
        <textarea class="form-control" id="comments" name="comments" placeholder="Opmerkingen bij project" rows="4"></textarea>
        <small id="Quotation" class="form-text text-muted"><i>Optioneel</i></small>
      </div>
    </div>

    <div class="col-md-5 offset-md-1">
      <div class="card">
        <h4 class="card-header">Samenvatting</h4>
        <div class="card-block">
          <div id="txtCustomer"></div>
        <div id="address"></div>
          <p class="card-text"><strong>Start- en einddatum:</strong><br>01/02/2017 - 03/04/2017</p>
          <p class="card-text"><strong>Status:</strong><br><span class="badge badge-default">Ingepland</span></p>
          <button type="submit" class="btn btn-primary btn-lg mt-3">Project aanmaken</button>
        </div>
      </div>
    </div>
  </div>
  </form>
	<script src='fetch.js'></script>
	<!-- Chosen Autofill JS/jQuery -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.js" type="text/javascript"></script>
	<script src="lib/chosen/chosen.jquery.js" type="text/javascript"></script>
	<script src="lib/chosen/docsupport/prism.js" type="text/javascript" charset="utf-8"></script>
	<script src="lib/chosen/docsupport/init.js" type="text/javascript" charset="utf-8"></script>
<?php
$conn->close();
?>
</body>
</html>
