<?php
include('security/dbconnection.php');
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Project Dybra</title>
	
	<!-- Chosen Autofill Style Sheets -->
	<link rel="stylesheet" href="lib/chosen/chosen.css">
	
    <!-- Bootstrap -->
    <?php
    include('header\css.php');
    ?>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script>
        function showUser(str) {
            if (str == "") {
                document.getElementById("txtHint").innerHTML = "";
                return;
            } else { 
                if (window.XMLHttpRequest) {
                    // code for IE7+, Firefox, Chrome, Opera, Safari
                    xmlhttp = new XMLHttpRequest();
                } else {
                    // code for IE6, IE5
                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                }
                xmlhttp.onreadystatechange = function() {
                    if (this.readyState == 4 && this.status == 200) {
                        document.getElementById("txtHint").innerHTML = this.responseText;
                    }
                };
                xmlhttp.open("GET","getcustomers.php?q="+str,true);
                xmlhttp.send();
            }
        }
    </script>
  </head>
  <body>
  <?php
  include('navigation.php');
  ?>


    <select data-placeholder="Selecteer klant" class="chosen-select" tabindex="2" onchange="showUser(this.value)">
        <option value=""></option>
            <?php
			$sql = "SELECT id, companyname, firstname, insertion, lastname FROM customers";
				$result = $conn->query($sql);
			echo "<optgroup label=\"Bedrijven\">";
			if ($result->num_rows > 0) {
				// output data of each row
				while($row = $result->fetch_assoc()) {
						if (!empty($row["companyname"]))echo "<option value=".$row["id"].">". $row["companyname"]."</option>";
				}
			} else {
				echo "0 results";
			}
			echo "</optgroup>";
			
			$sql2 = "SELECT id, companyname, firstname, insertion, lastname FROM customers";
				$result = $conn->query($sql);
			echo "<optgroup label=\"Particulieren\">";
			if ($result->num_rows > 0) {
				// output data of each row
				while($row2 = $result->fetch_assoc()) {
					if (empty($row2["companyname"])){ echo "<option value=".$row2["id"].">".$row2["firstname"]." ".$row2["insertion"]. " " . $row2["lastname"]."</option>";}
					//	else echo "<option value=".$row["id"].">". $row["companyname"]."</option>";
				}
			} else {
				echo "0 results";
			}
			echo "</optgroup>";
						
			$conn->close();
			?>
    </select>
<br>
<div id="txtHint"><b>Info will be listed here...</b></div>
	
	<!-- Chosen Autofill JS/jQuery -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.js" type="text/javascript"></script>
	<script src="lib/chosen/chosen.jquery.js" type="text/javascript"></script>
	<script src="lib/chosen/docsupport/prism.js" type="text/javascript" charset="utf-8"></script>
	<script src="lib/chosen/docsupport/init.js" type="text/javascript" charset="utf-8"></script>

</body>
</html>