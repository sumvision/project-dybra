<?php
include('security/dbconnection.php');
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Project Dybra</title>

    <!-- Bootstrap -->
    <!--<link href="assets/stylesheets/bootstrap.css" rel="stylesheet">-->
    <?php
    include('css.php');
    ?>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
  <?php
  include('navigation.php');	
  ?>

  <div class="container">
  <div class="row">
    <div class="col">

      <div class="card">
        <div class="card-block">
           <div class="row">
              <div class="col-md-6">
                <img src="https://s3-storage.textopus.nl/wp-content/uploads/2016/06/17221053/antwerpen-google-maps.png" height="300px" alt="">
              </div>
              <div class="col-md-6">
                  <blockquote class="card-blockquote">
                    <p>Jouw dag</p>
                    <footer>Vandaag werk je op project Van Veen.</footer>
                  </blockquote>
              </div>
            </div>
        </div>
      </div>
    </div>
  </div>

    <div class="row">
      <div class="col">
        <div class="card">
          <div class="card-block">
            <h4>Dagplanning</h4>
            <div class="table-responsive">
              <table class="table table-striped table-hover table-sm">
                <thead class="thead-default">
                  <tr>
                    <th>Voorman</th>
                    <th>Medewerker(s)</th>
                    <th>#</th>
                    <th>Klant</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>Mark van Driel</td>
                    <td>Juul Otter</td>
                    <td>242345</td>
                    <td>Hoepel</td>
                  </tr>
                  <tr>
                    <td>Mark van Driel</td>
                    <td>Juul Otter</td>
                    <td>242345</td>
                    <td>Hoepel</td>
                  </tr>
                  <tr>
                    <td>Mark van Driel</td>
                    <td>Juul Otter</td>
                    <td>242345</td>
                    <td>Hoepel</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>