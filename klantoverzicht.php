<?php
include('security/dbconnection.php');
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Project Dybra</title>

    <!-- Bootstrap -->
    <?php
    include('css.php');
    ?>
    <?php
    include('fonts.php');
    ?>
    
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
  <?php
  include('navigation.php');
  ?>

  <div class="container">
    <div class="row">
      <div class="col-md-3">
        <h2 class="sr-only">Filter</h2>
        <form>
          <div class="form-group">
            <label for="formGroupExampleInput">Bedrijf</label>
            <input type="text" class="form-control" id="formGroupExampleInput" placeholder="">
          </div>
          <div class="form-group">
            <label for="formGroupExampleInput2">Adres</label>
            <input type="text" class="form-control" id="formGroupExampleInput2" placeholder="">
          </div>
          <div class="form-group">
            <label for="formGroupExampleInput2">Telefoonnummer</label>
            <input type="text" class="form-control" id="formGroupExampleInput2" placeholder="">
          </div>
          <div class="form-group">
            <label for="formGroupExampleInput2">E-mail</label>
            <input type="text" class="form-control" id="formGroupExampleInput2" placeholder="">
          </div>
          <button type="button" class="btn btn-primary">Update resultaten</button>
        </form>
      </div>
      <div class="col-md-9">
        <div class="card">
          <div class="card-block">
            <h2 class="sr-only">Overzicht</h2>
            <div class="table-responsive">
              <table class="table table-striped table-hover table-sm">
                <thead>
                    <th>Klant <span style="font-size: 12px;" class="glyphicon glyphicon-sort"></span></th>
                    <th>Adres <span style="font-size: 12px;" class="glyphicon glyphicon-sort"></span></th>
                    <th>Telefoonnummer</th>
                    <th>E-mailadres</th>
                </thead>
                <tbody>
                    <tr>
                      <td>Klaver & Co</td>
                      <td>Lylantsebaan 1, 2908 LG, Capelle aan den Ijssel</td>
                      <td>+31 10-4567892</td>
                      <td>helloworld@klaverbaas.nl</td>
                    </tr>
                </tbody>
              </table>
            </div>

            <div class="row">
              <div class="col">
                <?php
                include('pagination.php');
                ?>
              </div>
            </div>
          
          </div>
        </div>
        
      </div>
    </div>
  </div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>