<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Project Dybra</title>

    <!-- Bootstrap -->
    <?php
    include('css.php');
    ?>
    <?php
    include('fonts.php');
    ?>
    
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
  <?php
  include('navigation.php');
  ?>
  <div class="container">
    <div class="row">
      <div class="col-md-6">
        <h1>Nieuw project</h1>
        <form>
          <div class="form-group">
            <label for="exampleInputEmail1">Klant</label>
            <div class="input-group">
              <input type="text" class="form-control" placeholder="Selecteer klant">
              <span class="input-group-btn">
                <button class="btn btn-primary" type="button">
                  <span style="font-size: 12px;" class="glyphicon glyphicon-plus"></span>
                  Nieuwe klant
                </button>
              </span>
            </div>
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Werklocatie</label><br>
            <!-- When no client is selected, show location unknown -->
            <span class="text-muted">Onbekend</span><br><br>
            <!-- After client selection, show address -->
            <span>Kleiweg 27, 3000 AA<br>Rotterdam</span><br>
            <a href="#" class="form-text small"><span style="font-size: 10px;" class="glyphicon glyphicon-pencil"></span> Werklocatie wijzigen</a>
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Voorman</label>
            <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Selecteer voorman">
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Start- en einddatum</label><br>
            <div class="form-inline">
              <input type="date" class="form-control">
              <span>-</span>
              <input type="date" class="form-control">
            </div>
          </div>
          <div class="form-group">
            <label for="formGroupExampleInput5">Status</label><br>
              <select class="form-control" id="exampleSelect1">
                <option>Ingepland</option>
                <option>Lopend</option>
                <option>Vertraagd</option>
                <option>Stopgezet</option>
                <option>Afgerond</option>
              </select>
              <!--<div class="btn-group" data-toggle="buttons">
                <label class="btn btn-default active">
                  <input type="checkbox" autocomplete="off"> Ingepland
                </label>
                <label class="btn btn-default">
                  <input type="checkbox" checked autocomplete="off"> Lopend
                </label>
                <label class="btn btn-default">
                  <input type="checkbox" autocomplete="off"> Vertraagd
                </label>
                <label class="btn btn-default">
                  <input type="checkbox" autocomplete="off"> Stopgezet
                </label>
                <label class="btn btn-default">
                  <input type="checkbox" autocomplete="off"> Afgerond
                </label>
              </div>-->
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Voorcalculatie</label><br>
            <!--<button class="btn btn-default">Maak voorcalculatie</button><br>-->
            <button class="btn btn-default"><span style="font-size: 12px;" class="glyphicon glyphicon-eye-open"></span> Bekijk calculatie</button>
            <button class="btn btn-default"><span style="font-size: 12px;" class="glyphicon glyphicon-download-alt"></span> Download calculatie</button>
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Offerte</label><br>
            <button class="btn btn-primary"><span style="font-size: 12px;" class="glyphicon glyphicon-pencil"></span> Maak offerte</button>
            <small class="form-text text-muted"><i>Optioneel</i></small><br>
          </div>
          <div class="form-group">
            <label for="exampleTextarea">Opmerkingen</label>
            <textarea class="form-control" id="exampleTextarea" placeholder="Opmerkingen bij project" rows="4"></textarea>
          </div>
           <!--<button type="submit" class="btn btn-success">Project aanmaken</button>-->
        </form><br><br>
      </div>

      <div class="col-md-5 col-md-offset-1">
        <div class="">
          <h2 class="h4">Klantinformatie</h2>
            <span>Van Veen<br>
            Kleiweg 27, 3000 AA<br>
            Rotterdam</span>

          <h2 class="h4">Voorman</h2>
          <span>Karel B</span>

          <h2 class="h4">Data</h2>
          <span>01-01-2015</span> - <span>21-05-2015</span>

          <h2 class="h4">Status</h2>
          <span>Lopend</span>

          <h2 class="h4">Voorcalculatie</h2>
          <button class="btn btn-default"><span style="font-size: 12px;" class="glyphicon glyphicon-eye-open"></span> Bekijk</button>

          <h2 class="h4">Offerte</h2>
          <span>Nog niet aangemaakt</span>

          <h2 class="h4">Opmerkingen</h2>
          <p>Blabla.</p>

          <button type="button" class="btn btn-success btn-lg btn-block"><span class="glyphicon glyphicon-ok"></span> Project aanmaken</button>
        </div>
      </div>
    </div>
  </div>
</body>
</html>