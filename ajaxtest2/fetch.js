$('#customer').change(function(){
	$.getJSON(
		'fetch.php',
		'customerid='+$('#customer').val(),
		function(result){
			$('#customername').empty();
			$('#address').empty();
			$.each(result.result, function(){
				$('#customername').append(this['customername']);
				$('#address').append(this['address']);
			});
		}
	);
});