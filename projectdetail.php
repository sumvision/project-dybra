<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Project Dybra</title>

    <!-- Bootstrap -->
    <?php
    include('css.php');
    ?>
    <?php
    include('fonts.php');
    ?>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
  <?php
  include('navigation.php');
  
  
include('security/dbconnection.php');

$projectid = ($_GET["projectid"]);

	$sql = "SELECT projectid, customerid, foreman, status, calculated_hours, startdate, enddate, comments FROM project_details WHERE projectid = $projectid";
		$result = $conn->query($sql);
		$row = $result->fetch_assoc();
			
$foreman = $row["foreman"];

	$sql2 = "SELECT firstname, insertion, lastname FROM workforce WHERE id = $foreman";
		$result2 = $conn->query($sql2);
		$row2 = $result2->fetch_assoc();	
		
$status = $row["status"];	

	$sql3 = "SELECT status FROM status WHERE id = $status";
		$result3 = $conn->query($sql3);
		$row3 = $result3->fetch_assoc();	
		
$customerid = $row["customerid"];
	
	$sql4 = "SELECT companyname, firstname, insertion, lastname, address, zipcode, city FROM customers WHERE id = $customerid";
		$result4 = $conn->query($sql4);
		$row4 = $result4->fetch_assoc();
			
echo"
  <div class=\"container\">

    <div class=\"row mb-4\">
      <div class=\"col\">
        <h1>"; if (empty($row4["companyname"])){ echo ucfirst($row4["insertion"]). " " . $row4["lastname"]." ".$row["projectid"]."</h1>";} else echo $row4["companyname"]." ".$row["projectid"]."</h1>";
echo"
      </div>
    </div>";
	
			
		
echo"
    <div class=\"row mb-4\">
      <div class=\"col-md-4\">
        <h2 class=\"h4\">Klantinformatie</h2>
        <span>
"; 

		if (empty($row4["companyname"])){ echo ucfirst($row4["insertion"]). " " . $row4["lastname"]."<br>";} else echo $row4["companyname"]."<br>";
		// Insert work location, if not available: show office location
echo
        $row4["address"]."<br>"
         .$row4["zipcode"].", ".$row4["city"]."</span>
      </div>
";	  
echo"
      <div class=\"col-md-4\">
        <h2 class=\"h4\">Voorman</h2>
        <span>".$row2["firstname"]." ".$row2["insertion"]." ".$row2["lastname"]."</span>
      </div>
      <div class=\"col-md-4\">
        <h2 class=\"h4\">Status</h2>
        <span class=\"badge badge-primary text-right\">".$row3["status"]."</span>
      </div>
    </div>
    <div class=\"row mb-4\">
      <div class=\"col-md-4\">
        <h2 class=\"h4\">Calculaties</h2>
        <div class=\"list-group\">
          <a href=\"#\" class=\"list-group-item list-group-item-action p-2 pl-3\">Offerte</a>
          <a href=\"#\" class=\"list-group-item list-group-item-action p-2 pl-3\">Voorcalculatie</a>
          <a href=\"#\" class=\"list-group-item list-group-item-action p-2 pl-3 disabled\">Nacalculatie</a>
        </div>
      </div>
      <div class=\"col-md-4\">
        <h2 class=\"h4\">Uren</h2>
        <span><b>Ingeschat:</b> ".$row["calculated_hours"]." uur<br>
        <b>Overwerk:</b> 3,3 uur (sum)
        </span>
      </div>
      <div class=\"col-md-4\">
        <h2 class=\"h4\">Opmerkingen</h2>
        <p class=\"text-muted\">Geen opmerkingen toegevoegd.</p>
      </div>
    </div>
";
?>

    <div class="row">
      <div class="col-md-4">
        <h2 class="h4">Agenda</h2>
        <div class="table-responsive">
          <table class="table table-sm">
            <tbody>
              <tr class="table-active">
                <td colspan="2">1 januari 2017</td>
              </tr>
              <tr>
                <td><span class="badge badge-default">11:00 - 12:30</span></td>
                <td>Levering lampen</td>
              </tr>
              <tr class="table-active">
                <td colspan="2">2 januari 2017</td>
              </tr>
              <tr>
                <td><span class="badge badge-default">14:00 - 17:30</span></td>
                <td>Levering lunch voor klant</td>
              </tr>
              <tr>
                <td><span class="badge badge-default">9:40 - 10:20</span></td>
                <td>Levering lampen</td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
      <div class="col-md-8">
        <h2 class="h4">Bestellingen</h2>
        <div class="table-responsive">
          <table class="table table-striped table-hover">
            <thead>
              <th>Leverancier <span style="font-size: 12px;" class="glyphicon glyphicon-sort"></span></th>
              <th>Product <span style="font-size: 12px;" class="glyphicon glyphicon-sort"></span></th>
              <th>Aantal <span style="font-size: 12px;" class="glyphicon glyphicon-sort"></span></th>
              <th>Eenheid</th>
            </thead>
            <tbody>
              <tr>
                <td>Hakkers B.V.</td>
                <td>Gelakte Rabat planken 400x20x2cm weerbestendig</td>
                <td>35</td>
                <td>m2</td>
              </tr>
              <tr>
                <td>Hakkers B.V.</td>
                <td>Partytent</td>
                <td>2</td>
                <td>st</td>
              </tr>
              <tr>
                <td>Hakkers B.V.</td>
                <td>Gelakte Rabat planken 400x20x2cm weerbestendig</td>
                <td>35</td>
                <td>m2</td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>