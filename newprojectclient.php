<?php
include('security/dbconnection.php');
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Project Dybra</title>

    <!-- Bootstrap -->
    <?php
    include('css.php');
    ?>
    <?php
    include('fonts.php');
    ?>
	
	  <!-- Chosen Autofill Style Sheets -->
  	<link rel="stylesheet" href="lib/chosen/chosen.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.js" type="text/javascript"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>

  <div class="container">
    <div class="row">
      <div class="col-md-6">
      <span class="float-right"><button class="btn btn-secondary btn-sm mt-1">Sluiten</button></span>
      <!-- START FORM -->
        <form class="mt-5">
          <div class="form-group row">
            <label for="example-text-input" class="col-3 col-form-label">Voornaam</label>
            <div class="col-9">
              <input class="form-control" type="text" placeholder="Jan" id="example-text-input">
            </div>
          </div>
          <div class="form-group row">
            <label for="example-search-input" class="col-3 col-form-label">Tussenvoegsel</label>
            <div class="col-9">
              <input class="form-control" type="text" placeholder="van der" id="example-text-input">
            </div>
          </div>
          <div class="form-group row">
            <label for="example-email-input" class="col-3 col-form-label">Achternaam</label>
            <div class="col-9">
              <input class="form-control" type="text" placeholder="Wulst" id="example-text-input">
            </div>
          </div>
          <div class="form-group row">
            <label for="example-url-input" class="col-3 col-form-label">Bedrijfsnaam</label>
            <div class="col-9">
              <input class="form-control" type="text" placeholder="Wulst BV" id="example-text-input">
            </div>
          </div>
          <div class="form-group row">
            <label for="example-url-input" class="col-3 col-form-label">Straat en huisnummer</label>
            <div class="col-9">
              <input class="form-control" type="text" placeholder="Dorpsplein 312b" id="example-text-input">
            </div>
          </div>
          <div class="form-group row">
            <label for="example-url-input" class="col-3 col-form-label">Postcode</label>
            <div class="col-9">
              <input class="form-control" type="text" placeholder="1234 AB" id="example-text-input">
            </div>
          </div>
          <div class="form-group row">
            <label for="example-url-input" class="col-3 col-form-label">Stad</label>
            <div class="col-9">
              <input class="form-control" type="text" placeholder="Gouda" id="example-text-input">
            </div>
          </div>
          <div class="form-group row">
            <label for="example-number-input" class="col-3 col-form-label">Telefoonnummer</label>
            <div class="col-9">
              <input class="form-control" type="number" placeholder="010-1234567" id="example-number-input">
            </div>
          </div>
          <div class="form-group row">
            <label for="example-email-input" class="col-3 col-form-label">Email</label>
            <div class="col-9">
              <input class="form-control" type="email" placeholder="info@wulstbv.com" id="example-email-input">
            </div>
          </div>
          <div class="form-group">
            <label for="exampleTextarea">Opmerkingen</label>
            <textarea class="form-control" id="exampleTextarea" rows="4"></textarea>
          </div>
        </form>
        <!-- END FORM -->
        <button type="submit" class="btn btn-primary float-right mt-3">Voeg klant toe</button>
    </div>
  </div>
	<script src='fetch.js'></script>
	<!-- Chosen Autofill JS/jQuery -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.js" type="text/javascript"></script>
	<script src="lib/chosen/chosen.jquery.js" type="text/javascript"></script>
	<script src="lib/chosen/docsupport/prism.js" type="text/javascript" charset="utf-8"></script>
	<script src="lib/chosen/docsupport/init.js" type="text/javascript" charset="utf-8"></script>

</body>
</html>