<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Project Dybra</title>

    <!-- Bootstrap -->
    <?php
    include('css.php');
    ?>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
  <?php
  include('navigation.php');
  ?>
  <div class="container">
    <div class="row">
      <div class="col-md-3">
        <h2 class="sr-only">Filter</h2>
        <form>
          <div class="form-group">
            <label for="formGroupExampleInput">Klantnaam</label>
            <input type="text" class="form-control" id="formGroupExampleInput" placeholder="">
          </div>
          <div class="form-group">
            <label for="formGroupExampleInput2">Adres</label>
            <input type="text" class="form-control" id="formGroupExampleInput2" placeholder="">
          </div>
          <div class="form-group">
            <label for="formGroupExampleInput">Product</label>
            <input type="text" class="form-control" id="formGroupExampleInput" placeholder="">
          </div>
          <div class="form-group">
            <label for="formGroupExampleInput2">Bestelcode</label>
            <input type="text" class="form-control" id="formGroupExampleInput2" placeholder="">
          </div>
          <div class="form-group">
            <label for="formGroupExampleInput5">Status</label>
            <select class="form-control" id="exampleSelect1">
              <option>Alle</option>
              <option>Afgeleverd</option>
              <option>Verwacht</option>
              <option>Vertraagd</option>
            </select>
          </div>
          <button type="button" class="btn btn-primary">Update resultaten</button>
        </form>
      </div>
      <div class="col-md-9">
        
        <div class="card">
          <div class="card-block">

            <h2 class="sr-only">Bestellingen</h2>
            <div class="table-responsive">
              <table class="table table-striped table-hover table-sm">
                <thead>
                  <th>Klant <span style="font-size: 12px;" class="glyphicon glyphicon-sort"></span></th>
                  <th>Projectcode <span style="font-size: 12px;" class="glyphicon glyphicon-sort"></span></th>
                  <th>Product <span style="font-size: 12px;" class="glyphicon glyphicon-sort"></span></th>
                  <th>Bestelcode <span style="font-size: 12px;" class="glyphicon glyphicon-sort"></span></th>
                  <th>Aantal <span style="font-size: 12px;" class="glyphicon glyphicon-sort"></span></th>
                  <th>Prijs <span style="font-size: 12px;" class="glyphicon glyphicon-sort"></span></th>
                  <th>Leverancier <span style="font-size: 12px;" class="glyphicon glyphicon-sort"></span></th>
                </thead>
                <tbody>
                  <tr>
                    <td>Van Veen</td>
                    <td>09V2798G3</td>
                    <td>Gelakte Rabat planken 400x20x2cm weerbestendig</td>
                    <td>783479032854095</td>
                    <td>30</td>
                    <td>&euro;34</td>
                    <td>Hofman</td>
                  </tr>
                  <tr>
                    <td>Van Veen</td>
                    <td>09V2798G3</td>
                    <td>Gelakte Rabat planken 400x20x2cm weerbestendig</td>
                    <td>783479032854095</td>
                    <td>30</td>
                    <td>&euro;23</td>
                    <td>Hofman</td>
                  </tr>
                  <tr>
                    <td>Van Veen</td>
                    <td>09V2798G3</td>
                    <td>Gelakte Rabat planken 400x20x2cm weerbestendig</td>
                    <td>783479032854095</td>
                    <td>30</td>
                    <td>&euro;654</td>
                    <td>Hofman</td>
                  </tr>
                </tbody>
              </table>
            </div>

            <div class="row">
              <div class="col">
                <?php
                include('pagination.php');
                ?>
              </div>
            </div>

          </div>
        </div>

      </div>
    </div>
  </div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>