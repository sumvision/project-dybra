<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Project Dybra</title>

    <!-- Bootstrap -->
    <?php
    include('css.php');
    ?>
    <?php
    include('fonts.php');
    ?>
    
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
  <?php
  include('navigation.php');
  ?>

  <div class="container">
    <div class="row">
      <div class="col-md-3">
        <h2 class="sr-only">Filter</h2>
        <form>
          <div class="form-group">
            <label for="formGroupExampleInput">Projectnummer</label>
            <input type="text" class="form-control" id="formGroupExampleInput" placeholder="">
          </div>
          <div class="form-group">
            <label for="formGroupExampleInput">Klantnaam</label>
            <input type="text" class="form-control" id="formGroupExampleInput" placeholder="">
          </div>
          <div class="form-group">
            <label for="formGroupExampleInput2">Adres</label>
            <input type="text" class="form-control" id="formGroupExampleInput2" placeholder="">
          </div>
          <div class="form-group">
            <label for="formGroupExampleInput">Voorman</label>
            <input type="text" class="form-control" id="formGroupExampleInput" placeholder="">
          </div>
          <div class="form-group">
            <label for="formGroupExampleInput2">Datum</label>
            <input type="date" class="form-control">
          </div>
          <div class="form-group">
            <label for="formGroupExampleInput5">Status</label>
            <select class="form-control" id="exampleSelect1">
              <option>Alle</option>
              <option>Lopend</option>
              <option>Vertraagd</option>
              <option>Afgerond</option>
            </select>
          </div>
          <button type="button" class="btn btn-primary">Update resultaten</button>
        </form>
      </div>
      <div class="col-md-9">
        <span>Overzicht</span>
        <div class="card">
          <div class="card-block">

            <h2 class="sr-only">Overzicht</h2>
            <div class="table-responsive">
              <table class="table table-striped table-hover table-sm">
                <thead>
                    <th>Project Nr.</span></th>
                    <th>Klant</span></th>
                    <th>Adres</span></th>
                    <th>Voorman</span></th>
                    <th>Startdatum</span></th>
                    <th>Einddatum</span></th>
                </thead>
                <tbody>
                  <?php
				  include('security/dbconnection.php');
				  
                  /*?projectid=".$row["projectid"]."*/
                  $sql = "SELECT projectid, customerid, foreman, startdate, enddate FROM project_details";
                    $result = $conn->query($sql);
                  
                  if ($result->num_rows > 0) {
                    // output data of each row
                    while($row = $result->fetch_assoc()) {
                      $foreman = $row["foreman"];
                      
                      $sql2 = "SELECT firstname, insertion, lastname FROM workforce WHERE id = $foreman";
                        $result2 = $conn->query($sql2);
                        $row2 = $result2->fetch_assoc();
                        
                      $customerid = $row["customerid"];
                        
                      $sql3 = "SELECT companyname, insertion, lastname, address, zipcode, city FROM customers WHERE id = $customerid";
                        $result3 = $conn->query($sql3);
                        $row3 = $result3->fetch_assoc();
                                
                      echo "<tr onclick=\"location.href='projectdetail.php?projectid=".$row["projectid"]."'\"/>";
                      echo "<td>".$row["projectid"]."</td>";
                      echo "<td>"; if (empty($row3["companyname"])){ echo ucfirst($row3["insertion"]). " " . $row3["lastname"]."</td>";} else echo $row3["companyname"]. "</td>";
                      echo "<td>".$row3["address"].", ".$row3["zipcode"].", ".$row3["city"]."</td>";
                      echo "<td>".$row2["firstname"]./*" ".$row2["insertion"]." ".$row2["lastname"].*/"</td>";
                      echo "<td>".$row["startdate"]."</td>";
                      echo "<td>".$row["enddate"]."</td>";
                      echo "</tr>";
                    }
                  } else {
                    echo "0 results";
                  }
                  $conn->close();
                  ?>
                </tbody>
              </table>
            </div>  

            <div class="row">
              <div class="col">
                <?php
                include('pagination.php');
                ?>
              </div>
            </div>

          </div>
        </div>
      </div>
    </div>
  </div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>