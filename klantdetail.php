<?php
include('security/dbconnection.php');
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Project Dybra</title>

    <!-- Bootstrap -->
    <?php
    include('css.php');
    ?>
    <?php
    include('fonts.php');
    ?>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
  <?php
  include('navigation.php');	
  ?>
  <div class="container">

    <div class="row">
      <div class="col-md-6">
        <h1 class="h2">Van Veen</h1>
        <span style="font-size: 24px;">
        Lylantsebaan 1<br>
        2908 LG, Capelle aan den Ijssel
        </span><br>
        <a href="tel:+31645678902" style="font-size: 24px;">+31 105678902</a><br><br>
        <a href="http://maps.google.com" target="_blank" class="btn btn-primary"><span style="font-size: 12px;" class="glyphicon glyphicon-map-marker"></span> Open in Google Maps</a>
      </div>
      <div class="col-md-6">
        <h2 class="h3">Contactpersonen</h2>
        <span>
          <strong>Klaasje van Veen</strong> - Directeur<br>
          <a href="mailto:hello@hello.com">k.vanveen@vanveen.nl</a><br>
          <a href="tel:+31667890673">+31 107890520</a><br>
          <a href="tel:+31667890673">+31 667890673</a>
        </span><br><br>
        <span>
          <strong>Lucia van Veen</strong> - Office Manager<br>
          <a href="mailto:hello@hello.com">l.vanveen@vanveen.nl</a><br>
          <a href="tel:+31667890673">+31 667893556</a>
        </span><br><br>
        <span>
          <strong>Marciano Dello</strong> - Sales Manager<br>
          <a href="mailto:hello@hello.com">m.dello@vanveen.nl</a><br>
          <a href="tel:+31667890673">+31 667890634</a>
        </span><br><br>
      </div>
      <!--<div class="col-md-4">
        <h2 class="h3">Totaal projecten</h2>
        <div class="metric" style="font-size: 60px;">
            15
        </div>
      </div>-->
    </div>

    <div class="row">
      <div class="col">
        <h2 class="h3">Projecten</h2>
        <div class="table-responsive">
          <table class="table table-striped table-hover">
            <thead>
              <th>Projectnummer <span style="font-size: 12px;" class="glyphicon glyphicon-sort"></span></th>
              <th>Voorman <span style="font-size: 12px;" class="glyphicon glyphicon-sort"></span></th>
              <th>Startdatum <span style="font-size: 12px;" class="glyphicon glyphicon-sort"></span></th>
              <th>Einddatum <span style="font-size: 12px;" class="glyphicon glyphicon-sort"></span></th>
              <th>Status <span style="font-size: 12px;" class="glyphicon glyphicon-sort"></span></th>
            </thead>
            <tbody>
              <tr>
                <td>123456</td>
                <td>Iddo Vermeulen</td>
                <td>21-01-2017</td>
                <td>01-05-2017</td>
                <td>Lopend</td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>

  </div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>